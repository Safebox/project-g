﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Controllers;

namespace Project_G
{
	/// <summary>
	/// This is the main type for your game.
	/// </summary>
	public class Game1 : Game
	{
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;

		UnifontLoader ufl;
		RenderTarget2D topScreen;
		RenderTarget2D bottomScreen;
		RenderTarget2D topText;
		RenderTarget2D bottomText;

		public Game1()
		{
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
			// TODO: Add your initialization logic here
			graphics.PreferredBackBufferWidth = 256;
			graphics.PreferredBackBufferHeight = 192 * 2;
			graphics.ApplyChanges();
			base.Initialize();
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			// Create a new SpriteBatch, which can be used to draw textures.
			spriteBatch = new SpriteBatch(GraphicsDevice);

			ufl = new UnifontLoader(GraphicsDevice, Content.RootDirectory);
			topScreen = new RenderTarget2D(GraphicsDevice, 256, 192);
			bottomScreen = new RenderTarget2D(GraphicsDevice, 256, 192);
			topText = new RenderTarget2D(GraphicsDevice, 256, 192);
			bottomText = new RenderTarget2D(GraphicsDevice, 256, 192);

			PilotingPractice.TransponderMap = new RenderTarget2D(GraphicsDevice, 80, 80);
			PilotingPractice.GravimetryMap = new RenderTarget2D(GraphicsDevice, 80, 80);
			PilotingPractice.PlanetMap = new RenderTarget2D(GraphicsDevice, 80, 76);

			Resources.Pixel = Texture2D.FromStream(GraphicsDevice, new System.IO.FileStream(Content.RootDirectory + @"\Pix.png", System.IO.FileMode.Open));
			Resources.Cursor = Texture2D.FromStream(GraphicsDevice, new System.IO.FileStream(Content.RootDirectory + @"\Cursor.png", System.IO.FileMode.Open));
			Resources.Signs = Texture2D.FromStream(GraphicsDevice, new System.IO.FileStream(Content.RootDirectory + @"\Signs.png", System.IO.FileMode.Open));

			Resources.Stamp = Texture2D.FromStream(GraphicsDevice, new System.IO.FileStream(Content.RootDirectory + @"\Stamp.png", System.IO.FileMode.Open));

			Resources.BurnoutFormula = Texture2D.FromStream(GraphicsDevice, new System.IO.FileStream(Content.RootDirectory + @"\Burnout.png", System.IO.FileMode.Open));
			Resources.Corners = Texture2D.FromStream(GraphicsDevice, new System.IO.FileStream(Content.RootDirectory + @"\Corners.png", System.IO.FileMode.Open));

			Resources.Theory = (Utilities.STheory)Newtonsoft.Json.JsonConvert.DeserializeObject(System.IO.File.ReadAllText(Content.RootDirectory + @"\Data\Theory.json"), typeof(Utilities.STheory));
			Resources.Questions = (Utilities.SQuestionCollection)Newtonsoft.Json.JsonConvert.DeserializeObject(System.IO.File.ReadAllText(Content.RootDirectory + @"\Data\QuestionCollection.json"), typeof(Utilities.SQuestionCollection));
			Resources.Questions.ShuffleQuestions();

			Resources.Interface = Texture2D.FromStream(GraphicsDevice, new System.IO.FileStream(Content.RootDirectory + @"\Interface.png", System.IO.FileMode.Open));
			Resources.VelocIndicator = Texture2D.FromStream(GraphicsDevice, new System.IO.FileStream(Content.RootDirectory + @"\VelocIndicator.png", System.IO.FileMode.Open));
			Resources.TechnicalManual = Texture2D.FromStream(GraphicsDevice, new System.IO.FileStream(Content.RootDirectory + @"\TechnicalManual.png", System.IO.FileMode.Open));
			Resources.SolSystem = (Utilities.SStarSystem)Newtonsoft.Json.JsonConvert.DeserializeObject(System.IO.File.ReadAllText(Content.RootDirectory + @"\Data\SolSystem.json"), typeof(Utilities.SStarSystem));
			Resources.MarkSol = Resources.SolSystem["B_Sol"].Distance + (float)System.Math.Abs(Resources.SolSystem["B_Mercury"].Distance);
			Resources.MarkEarth = Resources.SolSystem["B_Earth"].Distance + (float)(System.Math.Abs(Resources.SolSystem["B_Earth"].Distance - Resources.SolSystem["B_Mars"].Distance) / 2f);
			Resources.MarkAsteroidBelt = Resources.SolSystem["B_Ceres"].Distance + (float)(System.Math.Abs(Resources.SolSystem["B_Ceres"].Distance - Resources.SolSystem["B_Jupiter"].Distance) / 2f);
			Resources.MarkJupSat = Resources.SolSystem["B_Jupiter"].Distance + (float)(System.Math.Abs(Resources.SolSystem["B_Jupiter"].Distance - Resources.SolSystem["B_Saturn"].Distance) / 2f);
			Resources.MarkSatUra = Resources.SolSystem["B_Saturn"].Distance + (float)(System.Math.Abs(Resources.SolSystem["B_Saturn"].Distance - Resources.SolSystem["B_Uranus"].Distance) / 2f);
			Resources.MarkUraNep = Resources.SolSystem["B_Uranus"].Distance + (float)(System.Math.Abs(Resources.SolSystem["B_Uranus"].Distance - Resources.SolSystem["B_Neptune"].Distance) / 2f);
			Resources.MarkPostNeptune = Resources.SolSystem["B_Eris"].Distance + (float)(System.Math.Abs(Resources.SolSystem["B_Neptune"].Distance - Resources.SolSystem["B_Eris"].Distance) / 2f);
			Resources.MapMarks = new float[] { Resources.MarkSol, Resources.MarkEarth, Resources.MarkAsteroidBelt, Resources.MarkJupSat, Resources.MarkSatUra, Resources.MarkUraNep, Resources.MarkPostNeptune };
			Resources.GravityMap = Content.Load<Effect>("GravityMap");
			Resources.GravityMap.Parameters["c"].SetValue(new Vector4[] { Safe64.DarkBlue1.ToVector4(), Safe64.Red1.ToVector4() });
			Resources.AlcubierreMetric = Content.Load<Effect>("AlcubierreMetric");
			Resources.AlcubierreMetric.Parameters["c"].SetValue(new Vector4[] { Safe64.DarkBlue1.ToVector4(), Safe64.Red1.ToVector4() });

			/*for (int o = 0; o < Resources.SolSystem.BodyPositions().Length; o++)
			{
				if (Resources.SolSystem.Bodies[o].Name == "B_Mercury")
				{
					Player.Position = Resources.SolSystem.BodyPositions()[o];
					//floating point error occuring, unable to compensate
				}
			}*/
			// TODO: use this.Content to load your game content here
		}

		/// <summary>
		/// UnloadContent will be called once per game and is the place to unload
		/// game-specific content.
		/// </summary>
		protected override void UnloadContent()
		{
			// TODO: Unload any non ContentManager content here
		}

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime)
		{
			if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
				Exit();

			// TODO: Add your update logic here
			Input.CursorRect = new Rectangle(Mouse.GetState().Position, new Point(1, 1));
			switch (Player.CurrentState)
			{
				case Player.GameplayState.MainMenu:
					MainMenu.InputMainMenu(bottomScreen);
					break;
				case Player.GameplayState.Theory:
					Study.InputStudy(bottomScreen);
					break;
				case Player.GameplayState.TheoryTest:
					TheoryTest.InputTheory(bottomScreen, this.IsActive);
					break;
				case Player.GameplayState.Piloting:
					PilotingPractice.InputPilotingPractice(bottomScreen, gameTime);
					break;
			}
			Input.LastMouseState = Mouse.GetState();
			Input.LastKeyboardState = Keyboard.GetState();
			base.Update(gameTime);
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime)
		{
			//Top Screen
			GraphicsDevice.SetRenderTarget(topText);
			GraphicsDevice.Clear(Color.Transparent);
			spriteBatch.Begin();
			switch (Player.CurrentState)
			{
				case Player.GameplayState.MainMenu:
					//none yet
					break;
				case Player.GameplayState.Theory:
					spriteBatch.DrawStudyTopText(ufl, topScreen);
					break;
				case Player.GameplayState.TheoryTest:
					spriteBatch.DrawTheoryTopText(ufl, topScreen);
					break;
				case Player.GameplayState.Piloting:
					spriteBatch.DrawPilotingPracticeTopText(ufl, topScreen);
					break;
			}
			spriteBatch.End();

			if (Player.CurrentState == Player.GameplayState.Piloting)
			{
				//Transponder Map
				GraphicsDevice.SetRenderTarget(PilotingPractice.TransponderMap);
				GraphicsDevice.Clear(Color.Black);
				spriteBatch.Begin();
				spriteBatch.Draw(Resources.Interface, new Vector2(40, 40), new Rectangle(16, 16, 16, 16), Safe64.Black4, 0, new Vector2(8, 8), 0.5f, SpriteEffects.None, 0);
				for (int b = 0; b < Resources.SolSystem.Bodies.Length; b++)
				{
					if (Resources.SolSystem.Bodies[b].Name.Substring(0, 2) == "S_")
					{
						Vector3 v = Resources.SolSystem.BodyPositions(new Vector2(Player.Position.X * (float)System.Math.Cos((Player.Rotation + 90) / 180f * 3.14f) + Player.Position.Y * (float)System.Math.Sin((Player.Rotation + 90) / 180f * 3.14f) - 40, -Player.Position.X * (float)System.Math.Sin((Player.Rotation + 90) / 180f * 3.14f) + Player.Position.Y * (float)System.Math.Cos((Player.Rotation + 90) / 180f * 3.14f) - 40), Player.Rotation + 90)[b];
						spriteBatch.Draw(Resources.Interface, new Vector2(v.X, v.Y), new Rectangle(16, 96, 16, 16), Safe64.Black4, 0, new Vector2(8, 8), 0.5f, SpriteEffects.None, 0);
						ufl.DrawString(spriteBatch, new Vector2(v.X + 4, v.Y + 4), Resources.SolSystem.Bodies[b].Name.Substring(2), Safe64.Black4, 0.5f);
					}
				}
				spriteBatch.End();

				//Gravimetry Map
				GraphicsDevice.SetRenderTarget(PilotingPractice.GravimetryMap);
				spriteBatch.Begin(samplerState: SamplerState.PointClamp, effect: (Player.AlcubierreActive == false ? Resources.GravityMap : Resources.AlcubierreMetric));
				if (Player.AlcubierreActive == false)
				{
					Resources.GravityMap.Parameters["p"].SetValue(Resources.SolSystem.BodyPositions(new Vector2(Player.Position.X * (float)System.Math.Cos((Player.Rotation + 90) / 180f * 3.14f) + Player.Position.Y * (float)System.Math.Sin((Player.Rotation + 90) / 180f * 3.14f), -Player.Position.X * (float)System.Math.Sin((Player.Rotation + 90) / 180f * 3.14f) + Player.Position.Y * (float)System.Math.Cos((Player.Rotation + 90) / 180f * 3.14f)), Player.Rotation + 90, new Vector2(0.5f, 0.5f), 40f));
				}
				spriteBatch.Draw(Resources.Pixel, new Rectangle(0, 0, PilotingPractice.GravimetryMap.Width, PilotingPractice.GravimetryMap.Height), Safe64.Black4);
				spriteBatch.End();

				//Planet Map
				GraphicsDevice.SetRenderTarget(PilotingPractice.PlanetMap);
				GraphicsDevice.Clear(Color.Transparent);
				spriteBatch.Begin();
				int distanceIndex = 0;
				float[] k = Resources.MapMarks;
				for (int d = 0; d < Resources.MapMarks.Length; d++)
				{
					float p = Player.Position.Length();
					if (Resources.MapMarks[d] > p)
					{
						distanceIndex = d;
						break;
					}
					else
					{
						distanceIndex = Resources.MapMarks.Length - 1;
					}
				}
				spriteBatch.Draw(Resources.Pixel, new Vector2(40, 40), Safe64.Yellow1);
				for (int a = 0; a < 4; a++)
				{
					Vector2 s = new Vector2((float)System.Math.Cos(a / 2f * 3.14f), (float)System.Math.Sin(a / 2f * 3.14f));
					spriteBatch.Draw(Resources.Pixel, new Vector2(40 + s.X, 40 + s.Y), Safe64.Yellow1);
				}
				//ufl.DrawString(spriteBatch, new Vector2(40 + 2 + 1, 40 + 2), Resources.SolSystem.Bodies[0].Name.Substring(2), Safe64.Black1, 0.75f);
				//ufl.DrawString(spriteBatch, new Vector2(40 + 2, 40 + 2 + 1), Resources.SolSystem.Bodies[0].Name.Substring(2), Safe64.Black1, 0.75f);
				//ufl.DrawString(spriteBatch, new Vector2(40 + 2 - 1, 40 + 2), Resources.SolSystem.Bodies[0].Name.Substring(2), Safe64.Black1, 0.75f);
				//ufl.DrawString(spriteBatch, new Vector2(40 + 2, 40 + 2 - 1), Resources.SolSystem.Bodies[0].Name.Substring(2), Safe64.Black1, 0.75f);
				//ufl.DrawString(spriteBatch, new Vector2(40 + 2, 40 + 2), Resources.SolSystem.Bodies[0].Name.Substring(2), Safe64.Black4, 0.75f);
				for (int b = 1; b < Resources.SolSystem.Bodies.Length; b++)
				{
					Utilities.SBody body = Resources.SolSystem.Bodies[b];
					if (body.Name.Substring(0, 2) == "B_" && body.Parent == "B_Sol")
					{
						Vector3 v = Resources.SolSystem.BodyPositions(Resources.MapMarks[distanceIndex])[b] * 40;
						/*Utilities.SBody q = new Utilities.SBody();
						foreach (Utilities.SBody p in Resources.SolSystem.Bodies)
						{
							if ((float)System.Math.Abs(Player.Position.Length() - p.Distance) <= 299792458)
							{
								Player.AlcubierreCooldown = new System.TimeSpan();
								Vector2 vp = new Vector2((float)System.Math.Cos(System.TimeSpan.FromTicks(System.DateTime.UtcNow.Ticks).TotalDays / (body.Orbit / 2f) * 3.14f) * body.Distance, (float)System.Math.Sin(System.TimeSpan.FromTicks(System.DateTime.UtcNow.Ticks).TotalDays / (body.Orbit / 2f) * 3.14f) * body.Distance);
								v = Resources.SolSystem.BodyPositions(vp, 0)[b] * 40;
								break;
							}
						}*/
						spriteBatch.Draw(Resources.Pixel, new Vector2(v.X + 40, v.Y + 40), Safe64.Black4);
						if ((distanceIndex > 1 ? body.Distance >= Resources.MapMarks[distanceIndex - 1] : true) && body.Distance <= Resources.MapMarks[distanceIndex])
						{
							ufl.DrawString(spriteBatch, new Vector2(v.X + 40 + 2 + 1, v.Y + 40 + 2), body.Name.Substring(2), Safe64.Black1, 0.75f);
							ufl.DrawString(spriteBatch, new Vector2(v.X + 40 + 2, v.Y + 40 + 2 + 1), body.Name.Substring(2), Safe64.Black1, 0.75f);
							ufl.DrawString(spriteBatch, new Vector2(v.X + 40 + 2 - 1, v.Y + 40 + 2), body.Name.Substring(2), Safe64.Black1, 0.75f);
							ufl.DrawString(spriteBatch, new Vector2(v.X + 40 + 2, v.Y + 40 + 2 - 1), body.Name.Substring(2), Safe64.Black1, 0.75f);
							ufl.DrawString(spriteBatch, new Vector2(v.X + 40 + 2, v.Y + 40 + 2), body.Name.Substring(2), Safe64.Black4, 0.75f);
						}
					}
				}
				Vector2 pv = new Vector2(Player.Position.X, Player.Position.Y) / Resources.MapMarks[distanceIndex] * (PilotingPractice.PlanetMap.Width / 2);
				spriteBatch.Draw(Resources.Pixel, new Rectangle((int)(pv.X + PilotingPractice.PlanetMap.Width / 2f), (int)(pv.Y + PilotingPractice.PlanetMap.Width / 2f), (int)System.Math.Pow(80, 2), 1), null, Safe64.Black2, Player.Rotation / 180f * 3.14f, new Vector2(0, 0), SpriteEffects.None, 0);
				spriteBatch.Draw(Resources.Pixel, pv + new Vector2(PilotingPractice.PlanetMap.Width / 2f, PilotingPractice.PlanetMap.Width / 2f), Safe64.Red1);
				ufl.DrawString(spriteBatch, new Vector2(1, 0), (distanceIndex + 1) + "x", Safe64.Black1, 0.75f);
				ufl.DrawString(spriteBatch, new Vector2(0, 1), (distanceIndex + 1) + "x", Safe64.Black1, 0.75f);
				ufl.DrawString(spriteBatch, new Vector2(-1, 0), (distanceIndex + 1) + "x", Safe64.Black1, 0.75f);
				ufl.DrawString(spriteBatch, new Vector2(0, -1), (distanceIndex + 1) + "x", Safe64.Black1, 0.75f);
				ufl.DrawString(spriteBatch, new Vector2(0, 0), (distanceIndex + 1) + "x", Safe64.Black4, 0.75f);
				spriteBatch.End();
			}

			GraphicsDevice.SetRenderTarget(topScreen);
			spriteBatch.Begin(samplerState: SamplerState.PointClamp);
			switch (Player.CurrentState)
			{
				case Player.GameplayState.MainMenu:
					GraphicsDevice.Clear(Color.Transparent);
					spriteBatch.DrawMainMenuTopText(ufl, topScreen);
					spriteBatch.DrawMainMenuTop(topScreen, topText);
					break;
				case Player.GameplayState.Theory:
					GraphicsDevice.Clear(Safe64.Brown3);
					spriteBatch.DrawStudyTop(topScreen, topText);
					break;
				case Player.GameplayState.TheoryTest:
					GraphicsDevice.Clear(Safe64.Brown3);
					spriteBatch.DrawTheoryTop(topScreen, topText);
					break;
				case Player.GameplayState.Piloting:
					GraphicsDevice.Clear(Safe64.White2);
					spriteBatch.DrawPilotingPracticeTop(topScreen, topText);
					break;
			}
			spriteBatch.End();

			//Bottom Screen
			GraphicsDevice.SetRenderTarget(bottomText);
			GraphicsDevice.Clear(Color.Transparent);
			spriteBatch.Begin();
			switch (Player.CurrentState)
			{
				case Player.GameplayState.MainMenu:
					spriteBatch.DrawMainMenuBottomText(ufl, bottomScreen);
					break;
				case Player.GameplayState.Theory:
					spriteBatch.DrawStudyBottomText(ufl, bottomScreen);
					break;
				case Player.GameplayState.TheoryTest:
					spriteBatch.DrawTheoryBottomText(ufl, bottomScreen);
					break;
				case Player.GameplayState.Piloting:
					spriteBatch.DrawPilotingPracticeBottomText(ufl, bottomScreen);
					break;
			}
			spriteBatch.End();

			GraphicsDevice.SetRenderTarget(bottomScreen);
			spriteBatch.Begin(samplerState: SamplerState.PointClamp);
			switch (Player.CurrentState)
			{
				case Player.GameplayState.MainMenu:
					GraphicsDevice.Clear(Safe64.Brown3);
					spriteBatch.DrawMainMenuBottom(bottomScreen, bottomText);
					break;
				case Player.GameplayState.Theory:
					GraphicsDevice.Clear(Safe64.Brown3);
					spriteBatch.DrawStudyBottom(bottomScreen, bottomText);
					break;
				case Player.GameplayState.TheoryTest:
					GraphicsDevice.Clear(Safe64.Brown3);
					spriteBatch.DrawTheoryBottom(bottomScreen, bottomText);
					break;
				case Player.GameplayState.Piloting:
					GraphicsDevice.Clear(Safe64.White1);
					spriteBatch.DrawPilotingPracticeBottom(bottomScreen, bottomText);
					break;
			}
			spriteBatch.End();

			//Both Screens
			GraphicsDevice.SetRenderTarget(null);
			GraphicsDevice.Clear(Safe64.Black1);
			spriteBatch.Begin(samplerState: SamplerState.PointClamp);
			spriteBatch.Draw(topScreen, new Vector2(0, 0), Safe64.Black4);
			spriteBatch.Draw(bottomScreen, new Vector2(0, graphics.PreferredBackBufferHeight / 2), Safe64.Black4);

			spriteBatch.Draw(Resources.Cursor, Mouse.GetState().Position.ToVector2() - new Vector2(9, 0), Safe64.Black1);
			spriteBatch.Draw(Resources.Cursor, Mouse.GetState().Position.ToVector2() - new Vector2(8, 1), Safe64.Black1);
			spriteBatch.Draw(Resources.Cursor, Mouse.GetState().Position.ToVector2() - new Vector2(7, 0), Safe64.Black1);
			spriteBatch.Draw(Resources.Cursor, Mouse.GetState().Position.ToVector2() - new Vector2(8, -1), Safe64.Black1);
			spriteBatch.Draw(Resources.Cursor, Mouse.GetState().Position.ToVector2() - new Vector2(8, 0), Safe64.Black4);
			spriteBatch.End();

			// TODO: Add your drawing code here

			base.Draw(gameTime);
		}
	}
}
