﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

Texture2D SpriteTexture;

float4 c[2];

sampler2D SpriteTextureSampler = sampler_state
{
	Texture = <SpriteTexture>;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR0;
	float2 TextureCoordinates : TEXCOORD0;
};

float4 MainPS(VertexShaderOutput input) : COLOR
{
    /*float r = sqrt(pow(0.5 - input.TextureCoordinates.x, 2) + pow(0.5 - input.TextureCoordinates.y, 2));
    float R = 3.14;
    float t = lerp(cos(r * R), -cos(r * R), input.TextureCoordinates.y);
    return lerp(c[0], c[1], t);*/

    /*float d = sqrt(pow(0.5 - input.TextureCoordinates.x, 2) + pow(0.5 - input.TextureCoordinates.y, 2));
    float t = sin((d / 2) * 3.14);
    return lerp(c[0], c[1] * (0.5 - input.TextureCoordinates.y), t);*/

    float v = 1;
    float r = sqrt(pow(8 * (0.5 - input.TextureCoordinates.x), 2) + pow(8 * (0.5 - input.TextureCoordinates.y) - 1, 2));
    float R = 3.14;
    float f = (tanh(r + R) - tanh(r - R)) / (2 * tanh(R));
    float s = (pow(v, 2) * pow(f, 2)) - 2 * v * f * input.TextureCoordinates.y;
    
    return lerp(c[0], c[1] * sign(s * 0.98), abs(s * 0.98));
}

technique SpriteDrawing
{
	pass P0
	{
		PixelShader = compile PS_SHADERMODEL MainPS();
	}
};