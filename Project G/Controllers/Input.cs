﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Controllers
{
	public static class Input
	{
		//Variables
		public static MouseState LastMouseState;
		public static KeyboardState LastKeyboardState;
		public static Rectangle CursorRect;
	}
}
