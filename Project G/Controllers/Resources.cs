﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework.Graphics;

using Utilities;

namespace Controllers
{
	public static class Resources
	{
		//Variables
		public static Texture2D Pixel;
		public static Texture2D Cursor;
		public static Texture2D Signs;

		public static Texture2D Stamp;
		
		public static Texture2D BurnoutFormula;
		public static Texture2D Corners;

		public static STheory Theory;
		public static SQuestionCollection Questions;

		public static Texture2D Interface;
		public static Texture2D VelocIndicator;
		public static Texture2D TechnicalManual;
		public static SStarSystem SolSystem;
		public static float MarkSol;
		public static float MarkEarth;
		public static float MarkAsteroidBelt;
		public static float MarkJupSat;
		public static float MarkSatUra;
		public static float MarkUraNep;
		public static float MarkPostNeptune;
		public static float[] MapMarks;
		public static Effect GravityMap;
		public static Effect AlcubierreMetric;
	}
}
