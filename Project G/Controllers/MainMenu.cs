﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Controllers
{
	public static class MainMenu
	{
		//Enums
		public enum MenuItems { Study, Theory, Practice, Test };

		//Variables
		public static MenuItems SelectedItem = MenuItems.Study;

		private static Rectangle[] buttons;
		private static Rectangle startButton;

		//Methods
		public static void InputMainMenu(RenderTarget2D bottomScreen)
		{
			buttons = new Rectangle[] {
			new Rectangle(bottomScreen.Width / 2, 192 + 24, 16 * 7, 16),
			new Rectangle(bottomScreen.Width / 2, 192 + 24 + 24, 16 * 7, 16),
			new Rectangle(bottomScreen.Width / 2, 192 + 24 + 48, 16 * 7, 16),
			new Rectangle(bottomScreen.Width / 2, 192 + 24 + 72, 16 * 7, 16)
			};
			startButton = new Rectangle(bottomScreen.Width - 16 - Resources.Stamp.Width, 192 + bottomScreen.Height - 16 - (Resources.Stamp.Height / 2), Resources.Stamp.Width, Resources.Stamp.Height / 2);

			for (int b = 0; b < buttons.Length; b++)
			{
				if (b < 3 || (b > 2 && Player.TheoryQuestionsCorrect >= 35))
				{
					if (Input.CursorRect.Intersects(buttons[b]) && (Input.LastMouseState.LeftButton == ButtonState.Pressed && Mouse.GetState().LeftButton == ButtonState.Released))
					{
						SelectedItem = (MenuItems)b;
					}
				}
			}

			if (Input.CursorRect.Intersects(startButton) && (Input.LastMouseState.LeftButton == ButtonState.Pressed && Mouse.GetState().LeftButton == ButtonState.Released))
			{
				Player.CurrentState = (Player.GameplayState)(SelectedItem + 1);
			}
		}

		public static void DrawMainMenuTop(this SpriteBatch spriteBatch, RenderTarget2D topScreen, RenderTarget2D topText)
		{
			spriteBatch.Draw(topText, new Vector2(0, 0), Safe64.Black4);
		}

		public static void DrawMainMenuBottom(this SpriteBatch spriteBatch, RenderTarget2D bottomScreen, RenderTarget2D bottomText)
		{
			spriteBatch.Draw(Resources.Pixel, new Rectangle(16, 16, 16 * 7, bottomScreen.Height - 32), Safe64.Peach1);
			spriteBatch.Draw(Resources.Pixel, new Rectangle(24, 24, 16 * 6, bottomScreen.Height - 48), Safe64.Black4);
			spriteBatch.Draw(Resources.Stamp, new Vector2(bottomScreen.Width - 16 - Resources.Stamp.Width, bottomScreen.Height - 16 - (Resources.Stamp.Height / 2)), new Rectangle(0, (Mouse.GetState().LeftButton == ButtonState.Pressed && Input.CursorRect.Intersects(startButton) ? Resources.Stamp.Height / 2 : 0), Resources.Stamp.Width, Resources.Stamp.Height / 2),  Safe64.Black4);
			spriteBatch.Draw(Resources.Pixel, new Rectangle(bottomScreen.Width / 2, 24, 16 * 7, 16), Safe64.LightBlue1);
			spriteBatch.Draw(Resources.Pixel, new Rectangle(bottomScreen.Width / 2, 24 + 24, 16 * 7, 16), Safe64.LightBlue1);
			spriteBatch.Draw(Resources.Pixel, new Rectangle(bottomScreen.Width / 2, 24 + 48, 16 * 7, 16), Safe64.LightBlue1);
			spriteBatch.Draw(Resources.Pixel, new Rectangle(bottomScreen.Width / 2, 24 + 72, 16 * 7, 16), (Player.TheoryQuestionsCorrect >= 45 ? Safe64.LightBlue1 : Safe64.LightBlue4));
			int offsetY = 0;
			switch (SelectedItem)
			{
				case MenuItems.Study:
					offsetY = 0;
					break;
				case MenuItems.Theory:
					offsetY = 24;
					break;
				case MenuItems.Practice:
					offsetY = 48;
					break;
				case MenuItems.Test:
					offsetY = 72;
					break;
			}
			spriteBatch.Draw(Resources.Pixel, new Rectangle(bottomScreen.Width / 2 - 8, 24 + offsetY, 8, 16), Safe64.LightBlue1);
			spriteBatch.Draw(bottomText, new Vector2(0, 0), Safe64.Black4);
		}

		public static void DrawMainMenuTopText(this SpriteBatch spriteBatch, UnifontLoader ufl, RenderTarget2D topScreen)
		{
			ufl.DrawString(spriteBatch, new Vector2(topScreen.Width / 2 - (8 * 3.5f), topScreen.Height / 2 - (8 * 3.5f)), "Ｇ", Safe64.Black4, 3.5f, true);
			ufl.DrawString(spriteBatch, new Vector2(topScreen.Width / 2 - (8 * 3.5f), topScreen.Height / 2 - (8 * 3.5f) - 16), "PROJECT", Safe64.Black4);
		}

		public static void DrawMainMenuBottomText(this SpriteBatch spriteBatch, UnifontLoader ufl, RenderTarget2D bottomScreen)
		{
			spriteBatch.DrawLabels(ufl, bottomScreen);
			switch (SelectedItem)
			{
				case MenuItems.Study:
					spriteBatch.DrawStudy(ufl);
					break;
				case MenuItems.Theory:
					spriteBatch.DrawTheory(ufl);
					break;
				case MenuItems.Practice:
					spriteBatch.DrawPractice(ufl);
					break;
			}
		}

		private static void DrawLabels(this SpriteBatch spriteBatch, UnifontLoader ufl, RenderTarget2D bottomScreen)
		{
			ufl.DrawString(spriteBatch, new Vector2(bottomScreen.Width / 2 + 8, 28), "Study", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(bottomScreen.Width / 2 + 8, 28 + 24), "Theory Test" + (Player.TheoryQuestionsCorrect > 0 ? " (" + Player.TheoryQuestionsCorrect + "/50)" : ""), Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(bottomScreen.Width / 2 + 8, 28 + 48), "Piloting Practice", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(bottomScreen.Width / 2 + 8, 28 + 72), "Piloting Test", Safe64.Black1, 0.5f);
		}

		private static void DrawStudy(this SpriteBatch spriteBatch, UnifontLoader ufl)
		{
			ufl.DrawString(spriteBatch, new Vector2(24 + (16 * 3f) - (2.5f * 4), 32), "Study", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(24 + (16 * 3f) - (2.5f * 4), 32), "_____", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 48), "Consists of two parts:", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 48 + 16), "• Interplanetary Code", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 48 + 24), "Laws and procedures\nthat pilots must abide\nby when travelling in\ninterplanteary space.", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 48 + 72), "• Broadcast Signs", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 48 + 80), "Signs and symbols that\ninforms pilots about\nthe region they are\ncurrently in.", Safe64.Black1, 0.5f);
		}

		private static void DrawTheory(this SpriteBatch spriteBatch, UnifontLoader ufl)
		{
			ufl.DrawString(spriteBatch, new Vector2(24 + (16 * 3f) - (5.5f * 4), 32), "Theory Test", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(24 + (16 * 3f) - (5.5f * 4), 32), "___________", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 48), "• One hour", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 48 + 16), "• Fourty questions", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 48 + 32), "• Multiple choice", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 48 + 48), "• First answer is\n  accepted", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 80 + 48), "   TABBING OUT WILL\n      RESULT IN\n   DISQUALIFICATION", Safe64.Red1, 0.5f);
		}

		private static void DrawPractice(this SpriteBatch spriteBatch, UnifontLoader ufl)
		{
			ufl.DrawString(spriteBatch, new Vector2(24 + (16 * 3f) - (4f * 4), 32), "Practice", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(24 + (16 * 3f) - (4f * 4), 32), "________", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 48), "Model:      Coleoptera", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 48 + 8), "Manu.:     Volksschiff", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 48 + 16), "Prod.:       2438-2503", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 48 + 32), "Power Output:    22 MW", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 48 + 48), "Thrust:    0.00125 γ/s", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 48 + 56), "Rot.Thr.: 4.36x10^-5 θ", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 48 + 64), "Velocity:       0.04 γ", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 48 + 80), "Wheelbase:     2400 mm", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 48 + 88), "Length:        4079 mm", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 48 + 96), "Width:         1539 mm", Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 48 + 104), "Mass:           800 kg", Safe64.Black1, 0.5f);
		}
	}
}
