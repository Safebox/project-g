﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Controllers
{
	public static class TheoryTest
	{
		//Variables
		private static int QuestionNumber = 0;
		private static Utilities.SQuestion CurrentQuestion;
		private static bool TabbedOut;

		private static Rectangle[] buttons;

		//Methods
		public static void InputTheory(RenderTarget2D bottomScreen, bool isActive)
		{
			buttons = new Rectangle[] {
			new Rectangle(28, 192 + (bottomScreen.Height / 5) - 12, 16, 16),
			new Rectangle(28, 192 + (bottomScreen.Height / 5) * 2 - 12, 16, 16),
			new Rectangle(28, 192 + (bottomScreen.Height / 5) * 3 - 12, 16, 16),
			new Rectangle(28, 192 + (bottomScreen.Height / 5) * 4 - 12, 16, 16)
			};

			if (QuestionNumber < 40 && TabbedOut == false)
			{
				CurrentQuestion = Resources.Questions.Questions[QuestionNumber];

				for (int b = 0; b < buttons.Length; b++)
				{
					if (Input.CursorRect.Intersects(buttons[b]) && (Input.LastMouseState.LeftButton == ButtonState.Pressed && Mouse.GetState().LeftButton == ButtonState.Released))
					{
						if (Resources.Questions.Questions[QuestionNumber].ShuffledAnswers[b] == Resources.Questions.Questions[QuestionNumber].Answers[0])
						{
							Player.TheoryQuestionsCorrect++;
						}
						QuestionNumber++;
					}
				}

				if (isActive == false)
				{
					TabbedOut = !isActive;
				}
			}
			else
			{
				if (Input.CursorRect.Intersects(new Rectangle(0, 192, 256, 192)) && (Input.LastMouseState.LeftButton == ButtonState.Pressed && Mouse.GetState().LeftButton == ButtonState.Released))
				{
					QuestionNumber = 0;
					TabbedOut = false;
					Player.TheoryQuestionsCorrect = 0;
					Resources.Questions.ShuffleQuestions();
					Player.CurrentState = Player.GameplayState.MainMenu;
				}
			}
		}

		public static void DrawTheoryTop(this SpriteBatch spriteBatch, RenderTarget2D topScreen, RenderTarget2D topText)
		{
			spriteBatch.Draw(Resources.Pixel, new Rectangle(16, 16, topScreen.Width - 32, topScreen.Height - 32), Safe64.Peach1);
			spriteBatch.Draw(Resources.Pixel, new Rectangle(24, 24, topScreen.Width - 48, topScreen.Height - 48), Safe64.Black4);

			spriteBatch.Draw(topText, new Vector2(0, 0), Safe64.Black4);
		}

		public static void DrawTheoryBottom(this SpriteBatch spriteBatch, RenderTarget2D bottomScreen, RenderTarget2D bottomText)
		{
			spriteBatch.Draw(Resources.Pixel, new Rectangle(16, 16, bottomScreen.Width - 32, bottomScreen.Height - 32), Safe64.Peach1);
			spriteBatch.Draw(Resources.Pixel, new Rectangle(24, 24, bottomScreen.Width - 48, bottomScreen.Height - 48), Safe64.Black4);

			spriteBatch.Draw(bottomText, new Vector2(0, 0), Safe64.Black4);
		}

		public static void DrawTheoryTopText(this SpriteBatch spriteBatch, UnifontLoader ufl, RenderTarget2D topScreen)
		{
			spriteBatch.DrawTopLabels(ufl, topScreen);
		}

		public static void DrawTheoryBottomText(this SpriteBatch spriteBatch, UnifontLoader ufl, RenderTarget2D bottomScreen)
		{
			spriteBatch.DrawBottomLabels(ufl, bottomScreen);
		}

		private static void DrawTopLabels(this SpriteBatch spriteBatch, UnifontLoader ufl, RenderTarget2D topScreen)
		{
			if (QuestionNumber < 40 && TabbedOut == false)
			{
				if (CurrentQuestion.Question != null)
				{
					int s = 32;
					string[] words = CurrentQuestion.Question.Split(' ');
					for (int w = 0; w < words.Length; w++)
					{
						if (words[w].Length + 1 > s)
						{
							s = 32 - words[w].Length;
							words[w] = '\n' + words[w];
						}
						else
						{
							s -= (words[w].Length + 1);
							if (w != 0)
							{
								words[w] = ' ' + words[w];
							}
						}
					}

					string[] k = string.Concat(words).Split('\n');
					ufl.DrawString(spriteBatch, new Vector2(topScreen.Width / 2 - (("Question " + (QuestionNumber + 1)).Length / 2f * 6), 192 / 2 - ((k.Length + 2) / 2f * 12)), "Question " + (QuestionNumber + 1), Safe64.Black1, 0.75f);
					for (int i = 0; i < k.Length; i++)
					{
						int l = k[i].Length;
						ufl.DrawString(spriteBatch, new Vector2(topScreen.Width / 2 - (l / 2f * 6), 192 / 2 - ((k.Length + 2) / 2f * 12) + ((i + 2) * 12)), k[i], Safe64.Black1, 0.75f);
					}
				}
			}
			else
			{
				string m = (Player.TheoryQuestionsCorrect >= 35 && TabbedOut == false ? "Congratulations" : "I'm Sorry");
				ufl.DrawString(spriteBatch, new Vector2(topScreen.Width / 2 - (m.Length / 2f * 6), 192 / 2 - 12), m, Safe64.Black1, 0.75f);
				ufl.DrawString(spriteBatch, new Vector2(topScreen.Width / 2 - ((Player.TheoryQuestionsCorrect + "/40").Length / 2f * 6), 192 / 2), (Player.TheoryQuestionsCorrect + "/40"), Safe64.Black1, 0.75f);
			}
		}

		private static void DrawBottomLabels(this SpriteBatch spriteBatch, UnifontLoader ufl, RenderTarget2D bottomScreen)
		{
			if (QuestionNumber < 40 && TabbedOut == false)
			{
				if (CurrentQuestion.Question != null)
				{
					for (int a = 0; a < 4; a++)
					{
						int s = 31;
						string[] words = CurrentQuestion.Answers[a].Split(' ');
						for (int w = 0; w < words.Length; w++)
						{
							if (words[w].Length + 1 > s)
							{
								s = 31 - words[w].Length;
								words[w] = '\n' + words[w];
							}
							else
							{
								s -= (words[w].Length + 1);
								if (w != 0)
								{
									words[w] = ' ' + words[w];
								}
							}
						}

						string k = string.Concat(words);
						ufl.DrawString(spriteBatch, new Vector2(28, (bottomScreen.Height / 5) * (a + 1) - 12), "☐  " + k, Safe64.Black1, 0.75f);
					}
				}
			}
			else if (TabbedOut)
			{
				string m = "You clicked out of the test area\nand have been disqualified.\n\nClick anywhere to return\nto the main menu...";
				string[] n = m.Split('\n');
				for (int k = 0; k < n.Length; k++)
				{
					ufl.DrawString(spriteBatch, new Vector2(bottomScreen.Width / 2 - (n[k].Length / 2f * 6), 192 / 2 - (n.Length / 2f * 12) + (k * 12)), n[k], Safe64.Black1, 0.75f);
				}
			}
			else
			{
				string m = (Player.TheoryQuestionsCorrect >= 35 ? "You have passed the\ntheory portion of the test!\n\nClick anywhere to return\nto the main menu..." : "You have not passed the\ntheory portion of the test!\n\nClick anywhere to return\nto the main menu...");
				string[] n = m.Split('\n');
				for (int k = 0; k < n.Length; k++)
				{
					ufl.DrawString(spriteBatch, new Vector2(bottomScreen.Width / 2 - (n[k].Length / 2f * 6), 192 / 2 - (n.Length / 2f * 12) + (k * 12)), n[k], Safe64.Black1, 0.75f);
				}
			}
		}

		public static void Reset()
		{
			QuestionNumber = 0;
			TabbedOut = false;
			Resources.Questions.ShuffleQuestions();
		}
	}
}
