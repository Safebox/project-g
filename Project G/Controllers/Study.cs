﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Controllers
{
	public static class Study
	{
		//Variables
		private static int Page = 0;

		private static Rectangle[] buttons;

		//Methods
		public static void InputStudy(RenderTarget2D bottomScreen)
		{
			buttons = new Rectangle[] {
			new Rectangle(24, 192 + bottomScreen.Height - 24 - 16, 16, 16),
			new Rectangle(bottomScreen.Width - 24 - 16, 192 + bottomScreen.Height - 24 - 16, 16, 16)
			};

			if (Input.CursorRect.Intersects(buttons[0]) && (Input.LastMouseState.LeftButton == ButtonState.Pressed && Mouse.GetState().LeftButton == ButtonState.Released))
			{
				switch (Page)
				{
					case 0:
						Player.CurrentState = Player.GameplayState.MainMenu;
						break;
					case 1:
					case 2:
						Page--;
						Page = Math.Max(0, Page);
						break;
				}
			}
			if (Input.CursorRect.Intersects(buttons[1]) && (Input.LastMouseState.LeftButton == ButtonState.Pressed && Mouse.GetState().LeftButton == ButtonState.Released))
			{
				switch (Page)
				{
					case 0:
					case 1:
						Page++;
						Page = Math.Min(2, Page);
						break;
					case 2:
						Player.CurrentState = Player.GameplayState.MainMenu;
						break;
				}
			}
		}

		public static void DrawStudyTop(this SpriteBatch spriteBatch, RenderTarget2D topScreen, RenderTarget2D topText)
		{
			spriteBatch.Draw(Resources.Pixel, new Rectangle(16, 16, topScreen.Width - 32, topScreen.Height - 32), Safe64.Peach1);
			spriteBatch.Draw(Resources.Pixel, new Rectangle(24, 24, topScreen.Width - 48, topScreen.Height - 48), Safe64.Black4);

			spriteBatch.Draw(topText, new Vector2(0, 0), Safe64.Black4);
		}

		public static void DrawStudyBottom(this SpriteBatch spriteBatch, RenderTarget2D bottomScreen, RenderTarget2D bottomText)
		{
			spriteBatch.Draw(Resources.Pixel, new Rectangle(16, 16, bottomScreen.Width - 32, bottomScreen.Height - 32), Safe64.Peach1);
			spriteBatch.Draw(Resources.Pixel, new Rectangle(24, 24, bottomScreen.Width - 48, bottomScreen.Height - 48), Safe64.Black4);

			spriteBatch.Draw(bottomText, new Vector2(0, 0), Safe64.Black4);

			if (buttons != null)
			{
				spriteBatch.Draw(Resources.Corners, new Vector2(buttons[0].X, buttons[0].Y - 192), new Rectangle(0, 16 - ((int)Math.Round(Page / 3f * 2f) * 16), Resources.Corners.Width / 2, Resources.Corners.Height / 2), (Page == 0 ? Safe64.Black1 : Safe64.Black4));
				spriteBatch.Draw(Resources.Corners, new Vector2(buttons[1].X, buttons[1].Y - 192), new Rectangle(16, ((int)Math.Floor(Page / 3f * 2f) * 16), Resources.Corners.Width / 2, Resources.Corners.Height / 2), (Page == 2 ? Safe64.Black1 : Safe64.Black4));
			}
		}

		public static void DrawStudyTopText(this SpriteBatch spriteBatch, UnifontLoader ufl, RenderTarget2D topScreen)
		{
			spriteBatch.DrawTopLabels(ufl, topScreen);
		}

		public static void DrawStudyBottomText(this SpriteBatch spriteBatch, UnifontLoader ufl, RenderTarget2D bottomScreen)
		{
			spriteBatch.DrawBottomLabels(ufl, bottomScreen);
		}

		private static void DrawTopLabels(this SpriteBatch spriteBatch, UnifontLoader ufl, RenderTarget2D topScreen)
		{
			KeyValuePair<string, string> kvp1 = Resources.Theory.Sections.ElementAt(Page * 6);
			KeyValuePair<string, string> kvp2 = Resources.Theory.Sections.ElementAt((Page * 6) + 1);
			KeyValuePair<string, string> kvp3 = Resources.Theory.Sections.ElementAt((Page * 6) + 2);
			int offsetY1 = 2;
			int offsetY2 = 4;
			for (int c = 0; c < kvp1.Value.Length; c++)
			{
				if (kvp1.Value[c] == '\n')
				{
					offsetY1++;
				}
			}
			for (int c = 0; c < kvp2.Value.Length; c++)
			{
				if (kvp2.Value[c] == '\n')
				{
					offsetY2++;
				}
			}
			ufl.DrawString(spriteBatch, new Vector2(topScreen.Width / 2 - (kvp1.Key.Length / 2 * 4), 28), kvp1.Key, Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 44), kvp1.Value, Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(topScreen.Width / 2 - (kvp2.Key.Length / 2 * 4), 44 + (offsetY1 * 8)), kvp2.Key, Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 44 + ((offsetY1 + 2) * 8)), kvp2.Value, Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(topScreen.Width / 2 - (kvp3.Key.Length / 2 * 4), 44 + (offsetY1 * 8) + (offsetY2 * 8)), kvp3.Key, Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 44 + ((offsetY1 + 2) * 8) + (offsetY2 * 8)), kvp3.Value, Safe64.Black1, 0.5f);

			switch (Page)
			{
				case 2:
					spriteBatch.Draw(Resources.Signs, new Vector2(topScreen.Width / 2 - 16, 44 + ((offsetY1 - 5) * 8)), new Rectangle(0, 0, 32, 32), Safe64.Black4);
					spriteBatch.Draw(Resources.Signs, new Vector2(topScreen.Width / 2 - 16, 44 + ((offsetY1 + 1) * 8)), new Rectangle(32, 0, 32, 32), Safe64.Black4);
					spriteBatch.Draw(Resources.Signs, new Vector2(topScreen.Width / 2 - 16, 44 + ((offsetY1 + 1) * 8) + (offsetY2 * 8)), new Rectangle(64, 0, 32, 32), Safe64.Black4);
					break;
			}
		}

		private static void DrawBottomLabels(this SpriteBatch spriteBatch, UnifontLoader ufl, RenderTarget2D bottomScreen)
		{
			KeyValuePair<string, string> kvp4 = Resources.Theory.Sections.ElementAt((Page * 6) + 3);
			KeyValuePair<string, string> kvp5 = Resources.Theory.Sections.ElementAt((Page * 6) + 4);
			KeyValuePair<string, string> kvp6 = Resources.Theory.Sections.ElementAt((Page * 6) + 5);
			int offsetY1 = 2;
			int offsetY2 = 4;
			for (int c = 0; c < kvp4.Value.Length; c++)
			{
				if (kvp4.Value[c] == '\n')
				{
					offsetY1++;
				}
			}
			for (int c = 0; c < kvp5.Value.Length; c++)
			{
				if (kvp5.Value[c] == '\n')
				{
					offsetY2++;
				}
			}
			ufl.DrawString(spriteBatch, new Vector2(bottomScreen.Width / 2 - (kvp4.Key.Length / 2 * 4), 28), kvp4.Key, Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 44), kvp4.Value, Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(bottomScreen.Width / 2 - (kvp5.Key.Length / 2 * 4), 44 + (offsetY1 * 8)), kvp5.Key, Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 44 + ((offsetY1 + 2) * 8)), kvp5.Value, Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(bottomScreen.Width / 2 - (kvp6.Key.Length / 2 * 4), 44 + (offsetY1 * 8) + (offsetY2 * 8)), kvp6.Key, Safe64.Black1, 0.5f);
			ufl.DrawString(spriteBatch, new Vector2(28, 44 + ((offsetY1 + 2) * 8) + (offsetY2 * 8)), kvp6.Value, Safe64.Black1, 0.5f);

			switch (Page)
			{
				case 0:
					spriteBatch.Draw(Resources.BurnoutFormula, new Vector2(bottomScreen.Width / 2 - Resources.BurnoutFormula.Width / 2, 44 + ((offsetY1 - 3) * 8)), Safe64.Black4);
					break;
				case 2:
					spriteBatch.Draw(Resources.Signs, new Vector2(bottomScreen.Width / 2 - 16, 44 + ((offsetY1 - 5) * 8)), new Rectangle(96, 0, 32, 32), Safe64.Black4);
					spriteBatch.Draw(Resources.Signs, new Vector2(bottomScreen.Width / 2 - 16, 44 + ((offsetY1 + 1) * 8)), new Rectangle(128, 0, 32, 32), Safe64.Black4);
					spriteBatch.Draw(Resources.Signs, new Vector2(bottomScreen.Width / 2 - 16, 44 + ((offsetY1 + 1) * 8) + (offsetY2 * 8)), new Rectangle(160, 0, 32, 32), Safe64.Black4);
					break;
			}
		}
	}
}
