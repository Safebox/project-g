﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Controllers
{
	public static class PilotingPractice
	{
		//Variables
		public static RenderTarget2D TransponderMap;
		public static RenderTarget2D GravimetryMap;
		public static RenderTarget2D PlanetMap;

		private static Rectangle[] switches = new Rectangle[4];
		private static bool[] stoggles = new bool[4] { true, false, false, false };
		private static Rectangle[] buttons = new Rectangle[2];
		private static bool[] btoggles = new bool[3] { false, false, false };
		private static Rectangle[] help = new Rectangle[3];
		private static bool htoggle = false;

		//Methods
		public static void InputPilotingPractice(RenderTarget2D bottomScreen, GameTime gameTime)
		{
			switches = new Rectangle[] {
			new Rectangle(bottomScreen.Width - 24, 192 + bottomScreen.Height - 32 - 48, 16, 24),
			new Rectangle(bottomScreen.Width - 24, 192 + bottomScreen.Height - 32, 16, 24),
			new Rectangle((bottomScreen.Width / 2), 192 + 0, 16, 24),
			new Rectangle((bottomScreen.Width / 2), 192 + 24, 16, 24)
			};

			buttons = new Rectangle[] {
			new Rectangle((bottomScreen.Width / 2), 192 + 56, 16, 24),
			new Rectangle((bottomScreen.Width / 2), 192 + 88, 16, 24),
			new Rectangle((bottomScreen.Width / 2), 192 + 72, 16, 24)
			};

			help = new Rectangle[] {
				new Rectangle(bottomScreen.Width - 16, 192, 16, 32),
				new Rectangle(bottomScreen.Width - 24 - 32, 192 + 24, 16, 16),
				new Rectangle(bottomScreen.Width - 24 - 16, 192 + 24, 16, 16)
			};

			if (htoggle == false)
			{
				for (int b = 0; b < switches.Length; b++)
				{
					if (Input.CursorRect.Intersects(switches[b]) && (Input.LastMouseState.LeftButton == ButtonState.Pressed && Mouse.GetState().LeftButton == ButtonState.Released))
					{
						switch (b)
						{
							case 2:
								if (stoggles[b + 1] == false)
								{
									stoggles[b] = !stoggles[b];
									Player.DisplacementActive = !Player.DisplacementActive;
								}
								break;
							case 3:
								if (stoggles[b - 1] == false && Player.AlcubierreActive == false)
								{
									stoggles[b] = !stoggles[b];
								}
								break;
							default:
								stoggles[b] = !stoggles[b];
								break;
						}
					}
				}

				if (stoggles[3])
				{
					for (int b = 0; b < buttons.Length; b++)
					{
						if (Input.CursorRect.Intersects(buttons[b]) && Input.LastMouseState.LeftButton == ButtonState.Pressed)
						{
							btoggles[b] = true;

							if (Mouse.GetState().LeftButton == ButtonState.Released)
							{
								if (Player.AlcubierreActive == false)
								{
									switch (b)
									{
										case 0:
											if (Player.AlcubierreDistance < 180)
											{
												Player.AlcubierreDistance++;
											}
											break;
										case 1:
											if (Player.AlcubierreDistance > 0)
											{
												Player.AlcubierreDistance--;
											}
											break;
										case 2:
											Player.AlcubierreCooldown = new TimeSpan(0, 0, Player.AlcubierreDistance);
											Player.Velocity = new Vector2(0, 299792458);
											Player.AlcubierreActive = true;
											break;
									}
								}
							}
						}
						else
						{
							if (b != 2)
							{
								btoggles[b] = false;
							}
						}
					}
				}

				if (Input.CursorRect.Intersects(help[0]) && (Input.LastMouseState.LeftButton == ButtonState.Pressed && Mouse.GetState().LeftButton == ButtonState.Released))
				{
					htoggle = true;
				}
			}
			else
			{
				if (Input.CursorRect.Intersects(help[1]) && (Input.LastMouseState.LeftButton == ButtonState.Pressed && Mouse.GetState().LeftButton == ButtonState.Released))
				{
					htoggle = false;
				}
				if (Input.CursorRect.Intersects(help[2]) && (Input.LastMouseState.LeftButton == ButtonState.Pressed && Mouse.GetState().LeftButton == ButtonState.Released))
				{
					Player.ResetPractical();
					Player.CurrentState = Player.GameplayState.MainMenu;
				}
			}

			if (Player.AlcubierreActive == false)
			{
				float k = (float)gameTime.ElapsedGameTime.TotalSeconds * 54;
				if (stoggles[2])
				{
					k = (float)(gameTime.ElapsedGameTime.TotalSeconds * (10 * Math.Pow(10, -3f)) * 299792458);
				}
				if (Keyboard.GetState().IsKeyDown(Keys.W))
				{
					Player.Velocity.Y += k;
				}
				else if (Keyboard.GetState().IsKeyDown(Keys.S))
				{
					Player.Velocity.Y -= k;
				}
				if (Keyboard.GetState().IsKeyDown(Keys.A))
				{
					Player.Velocity.X -= (float)gameTime.ElapsedGameTime.TotalSeconds * 2;
				}
				else if (Keyboard.GetState().IsKeyDown(Keys.D))
				{
					Player.Velocity.X += (float)gameTime.ElapsedGameTime.TotalSeconds * 2;
				}
				Player.Velocity = Vector2.Clamp(Player.Velocity, new Vector2(-4, -(0.4f * 299792458)), new Vector2(4, (0.4f * 299792458)));
			}
			Player.Rotation += Player.Velocity.X;
			Vector2 newPos = new Vector2(Player.Velocity.Y * (float)Math.Cos(Player.Rotation / 180f * 3.14f), Player.Velocity.Y * (float)Math.Sin(Player.Rotation / 180f * 3.14f));
			Player.Position += new Vector3(newPos, 0);
			if (Player.AlcubierreActive == false && Player.DisplacementActive == false)
			{
				Player.Velocity *= (float)Math.Pow(0.5f, gameTime.ElapsedGameTime.TotalSeconds / 0.5f);
			}
			else if (Player.DisplacementActive)
			{
				Player.Velocity.Y *= (float)Math.Pow(0.99f, gameTime.ElapsedGameTime.TotalSeconds / 0.99f);
				Player.Velocity.X *= (float)Math.Pow(0.5f, gameTime.ElapsedGameTime.TotalSeconds / 0.5f);
			}
			else if (Player.AlcubierreActive)
			{
				if (Player.AlcubierreCooldown.TotalSeconds > 0)
				{
					TimeSpan ts = Player.AlcubierreCooldown;
					Player.AlcubierreCooldown = Player.AlcubierreCooldown.Subtract(gameTime.ElapsedGameTime);
					ts = Player.AlcubierreCooldown;
				}
				else
				{
					Player.Velocity = new Vector2(0, 0);
					Player.AlcubierreActive = false;
					btoggles[2] = false;
				}
			}
		}

		public static void DrawPilotingPracticeTop(this SpriteBatch spriteBatch, RenderTarget2D topScreen, RenderTarget2D topText)
		{
			spriteBatch.Draw(TransponderMap, new Vector2(24, 24), Safe64.Black4);
			spriteBatch.Draw(GravimetryMap, new Vector2(topScreen.Width / 2 + 24, 24), Safe64.Black4);
			DrawTopBorder(spriteBatch, new Rectangle(16, 16, (topScreen.Width / 2) - 32, (topScreen.Width / 2) - 32));
			DrawTopBorder(spriteBatch, new Rectangle((topScreen.Width / 2) + 16, 16, (topScreen.Width / 2) - 32, (topScreen.Width / 2) - 32));

			//DrawTopBorder(spriteBatch, new Rectangle(16, topScreen.Height - ((topScreen.Height / 2) - 32), (topScreen.Width) - 32, (topScreen.Height / 2) - 48));

			spriteBatch.Draw(topText, new Vector2(0, 0), Safe64.Black4);
		}

		public static void DrawPilotingPracticeBottom(this SpriteBatch spriteBatch, RenderTarget2D bottomScreen, RenderTarget2D bottomText)
		{
			//Monitors
			DrawBottomBorder(spriteBatch, new Rectangle(8, bottomScreen.Height - 80 - 8, bottomScreen.Width - 40, 80), true);
			DrawBottomBorder(spriteBatch, new Rectangle((bottomScreen.Width / 2) + 24, -8, (bottomScreen.Width / 2) - 32, (bottomScreen.Width / 2) - 16), true);
			DrawBottomBorder(spriteBatch, new Rectangle(8, 48, (bottomScreen.Width / 2) - 16, 56), true);

			//Toggle Switches
			for (int b = 0; b < switches.Length; b++)
			{
				spriteBatch.Draw(Resources.Interface, new Vector2(switches[b].X, switches[b].Y - 192), new Rectangle(48, 0 + (stoggles[b] ? 24 : 0), switches[b].Width, 24), Safe64.Black4);
			}
			spriteBatch.Draw(Resources.Interface, new Vector2(switches[2].X - 24, 8), new Rectangle(48, 96, 16, 16), Safe64.Black4);
			spriteBatch.Draw(Resources.Interface, new Vector2(switches[2].X - 24, 8), new Rectangle(48, 112, 16, 16), (stoggles[2] && stoggles[0] ? Safe64.LightGreen2 : Safe64.LightGreen4));
			spriteBatch.Draw(Resources.Interface, new Vector2(switches[2].X - 24, 32), new Rectangle(48, 96, 16, 16), Safe64.Black4);
			spriteBatch.Draw(Resources.Interface, new Vector2(switches[2].X - 24, 32), new Rectangle(48, 112, 16, 16), (stoggles[3] && stoggles[0] ? Safe64.LightGreen2 : Safe64.LightGreen4));

			//Buttons
			for (int b = 0; b < buttons.Length; b++)
			{
				spriteBatch.Draw(Resources.Interface, new Vector2(buttons[b].X, buttons[b].Y - 192), new Rectangle(48, 96, 16, 16), Safe64.Black4);
				spriteBatch.Draw(Resources.Interface, new Vector2(buttons[b].X, buttons[b].Y - 192), new Rectangle(48, 96 + 32 + (btoggles[b] ? 16 : 0), 16, 16), (b < btoggles.Length - 1 ? Safe64.Red1 : Safe64.LightBlue1));
			}

			//GUI
			if (stoggles[0])
			{
				if (stoggles[1])
				{
					spriteBatch.Draw(Resources.Signs, new Vector2(24, bottomScreen.Height - 56), new Rectangle(0, 0, 32, 32), Safe64.Black4);
				}
				/*if (stoggles[2])
				{
					spriteBatch.Draw(Resources.VelocIndicator, new Vector2(bottomScreen.Width - 80, 32), new Rectangle(0, 0, 24, 24), Color.Lerp(Safe64.Red1, Safe64.DarkBlue1, 1 + MathHelper.Clamp(Player.Velocity.X, -1, 0)));
					spriteBatch.Draw(Resources.VelocIndicator, new Vector2(bottomScreen.Width - 56, 32), new Rectangle(24, 0, 24, 24), Color.Lerp(Safe64.DarkBlue1, Safe64.Red1, MathHelper.Clamp(Player.Velocity.X, 0, 1)));
					spriteBatch.Draw(Resources.VelocIndicator, new Vector2(bottomScreen.Width - 80, 56), new Rectangle(0, 24, 24, 24), Color.Lerp(Safe64.DarkBlue1, Safe64.Red1, MathHelper.Clamp(Player.Velocity.X, 0, 1)));
					spriteBatch.Draw(Resources.VelocIndicator, new Vector2(bottomScreen.Width - 56, 56), new Rectangle(24, 24, 24, 24), Color.Lerp(Safe64.Red1, Safe64.DarkBlue1, 1 + MathHelper.Clamp(Player.Velocity.X, -1, 0)));
					spriteBatch.Draw(Resources.VelocIndicator, new Vector2(bottomScreen.Width - 80, 32), new Rectangle(48, 0, 48, 24), Color.Lerp(Safe64.DarkBlue1, Safe64.Red1, MathHelper.Clamp(Player.Velocity.Y, 0, 1)));
					spriteBatch.Draw(Resources.VelocIndicator, new Vector2(bottomScreen.Width - 80, 56), new Rectangle(48, 24, 48, 24), Color.Lerp(Safe64.Red1, Safe64.DarkBlue1, 1 + MathHelper.Clamp(Player.Velocity.Y, -1, 0)));
				}
				else
				{*/
					spriteBatch.Draw(PlanetMap, new Vector2((bottomScreen.Width / 2) + 24 + 8, 16), Safe64.Black4);
				//}
			}

			//Labels
			spriteBatch.Draw(Resources.Pixel, new Rectangle(8, 8, (bottomScreen.Width / 2) - 40, 16), Safe64.White4);
			spriteBatch.Draw(Resources.Pixel, new Rectangle(8, 6, (bottomScreen.Width / 2) - 40, 16), Safe64.White3);
			spriteBatch.Draw(Resources.Pixel, new Rectangle(8, 32, (bottomScreen.Width / 2) - 40, 16), Safe64.White4);
			spriteBatch.Draw(Resources.Pixel, new Rectangle(8, 30, (bottomScreen.Width / 2) - 40, 16), Safe64.White3);

			//help[0]
			if (htoggle == false)
			{
				if (Input.CursorRect.Intersects(help[0]))
				{
					spriteBatch.Draw(Resources.Interface, new Rectangle(help[0].X, help[0].Y - 192, help[0].Width, help[0].Height), new Rectangle(16, 64 + 64, 16, 32), Safe64.Black4);
				}
				else
				{
					spriteBatch.Draw(Resources.Interface, new Rectangle(help[0].X, help[0].Y - 192, help[0].Width, help[0].Height), new Rectangle(32, 64 + 64, 16, 32), Safe64.Black4);
				}
			}
			else
			{
				spriteBatch.Draw(Resources.Pixel, new Rectangle(16, 16, bottomScreen.Width - 32, bottomScreen.Height - 32), Safe64.Peach1);
				spriteBatch.Draw(Resources.Pixel, new Rectangle(24, 24, bottomScreen.Width - 48, bottomScreen.Height - 48), Safe64.Black4);
				spriteBatch.Draw(Resources.TechnicalManual, new Vector2(0, 0), Safe64.Black4);
				spriteBatch.Draw(Resources.Interface, new Rectangle(help[1].X, help[1].Y - 192, help[1].Width, help[1].Height), new Rectangle(0, 128 + 16, 16, 16), Safe64.Black1);
				spriteBatch.Draw(Resources.Interface, new Rectangle(help[2].X, help[2].Y - 192, help[2].Width, help[2].Height), new Rectangle(0, 128, 16, 16), Safe64.Black1);
			}

			spriteBatch.Draw(bottomText, new Vector2(0, 0), Safe64.Black4);
		}

		public static void DrawPilotingPracticeTopText(this SpriteBatch spriteBatch, UnifontLoader ufl, RenderTarget2D topScreen)
		{
			spriteBatch.DrawTopLabels(ufl, topScreen);
		}

		public static void DrawPilotingPracticeBottomText(this SpriteBatch spriteBatch, UnifontLoader ufl, RenderTarget2D bottomScreen)
		{
			spriteBatch.DrawBottomLabels(ufl, bottomScreen);
		}

		private static void DrawTopLabels(this SpriteBatch spriteBatch, UnifontLoader ufl, RenderTarget2D topScreen)
		{
			spriteBatch.Draw(Resources.Pixel, new Rectangle(16, (topScreen.Height / 2) + 32, (topScreen.Width / 2) - 32, 16), Safe64.White3);
			spriteBatch.Draw(Resources.Pixel, new Rectangle(16, (topScreen.Height / 2) + 34, (topScreen.Width / 2) - 32, 16), Safe64.White4);
			ufl.DrawString(spriteBatch, new Vector2(19, (topScreen.Height / 2) + 36), "Transponder Map", Safe64.White3, 0.75f);

			spriteBatch.Draw(Resources.Pixel, new Rectangle((topScreen.Width / 2) + 16, (topScreen.Height / 2) + 32, (topScreen.Width / 2) - 32, 16), Safe64.White3);
			spriteBatch.Draw(Resources.Pixel, new Rectangle((topScreen.Width / 2) + 16, (topScreen.Height / 2) + 34, (topScreen.Width / 2) - 32, 16), Safe64.White4);
			ufl.DrawString(spriteBatch, new Vector2((topScreen.Width / 2) + 22, (topScreen.Height / 2) + 36), "Gravimetry Map", Safe64.White3, 0.75f);
		}

		private static void DrawBottomLabels(this SpriteBatch spriteBatch, UnifontLoader ufl, RenderTarget2D bottomScreen)
		{
			if (htoggle == false)
			{
				//Labels
				ufl.DrawString(spriteBatch, new Vector2(16, 7), "Displacement", Safe64.White2, 0.75f);
				ufl.DrawString(spriteBatch, new Vector2(22, 31), "Alcubierre", Safe64.White2, 0.75f);

				//Interfaces
				if (stoggles[0])
				{
					string s = (stoggles[2] ? "Disp: " + (Player.Velocity.Y / 299792458f).ToString("+0.000;-0.000;0.000") + "γ" : (stoggles[3] ? "Alcu:   " + (Player.AlcubierreActive == false ? Player.AlcubierreDistance : Player.AlcubierreCooldown.TotalSeconds).ToString("00") + "lm" : "Thrust: " + Math.Min(Player.Velocity.Y, 99).ToString("+00;-00;00") + "m/s"));
					ufl.DrawString(spriteBatch, new Vector2(24, 48 + 32), s, Safe64.White2, 0.75f);
				}
			}
		}

		private static void DrawTopBorder(SpriteBatch spriteBatch, Rectangle area, bool background = false)
		{
			//Background
			if (background)
			{
				spriteBatch.Draw(Resources.Pixel, new Rectangle(area.X + 8, area.Y + 8, area.Width - 16, area.Height - 16), Safe64.Black1);
			}

			//Corners
			spriteBatch.Draw(Resources.Interface, new Vector2(area.X, area.Y), new Rectangle(0, 0, 16, 16), Safe64.Black4);
			spriteBatch.Draw(Resources.Interface, new Vector2(area.X + area.Width - 16, area.Y), new Rectangle(32, 0, 16, 16), Safe64.Black4);
			spriteBatch.Draw(Resources.Interface, new Vector2(area.X, area.Y + area.Height - 16), new Rectangle(0, 32, 16, 32), Safe64.Black4);
			spriteBatch.Draw(Resources.Interface, new Vector2(area.X + area.Width - 16, area.Y + area.Height - 16), new Rectangle(32, 32, 16, 32), Safe64.Black4);

			//Borders
			spriteBatch.Draw(Resources.Interface, new Rectangle(area.X + 16, area.Y, area.Width - 32, 16), new Rectangle(16, 0, 16, 16), Safe64.Black4);
			spriteBatch.Draw(Resources.Interface, new Rectangle(area.X, area.Y + 16, 16, area.Height - 32), new Rectangle(0, 16, 16, 16), Safe64.Black4);
			spriteBatch.Draw(Resources.Interface, new Rectangle(area.X + 16, area.Y + area.Height - 16, area.Width - 32, 32), new Rectangle(16, 32, 16, 32), Safe64.Black4);
			spriteBatch.Draw(Resources.Interface, new Rectangle(area.X + area.Width - 16, area.Y + 16, 16, area.Height - 32), new Rectangle(32, 16, 16, 16), Safe64.Black4);
		}

		private static void DrawBottomBorder(SpriteBatch spriteBatch, Rectangle area, bool background = false)
		{
			//Background
			if (background)
			{
				spriteBatch.Draw(Resources.Pixel, new Rectangle(area.X + 8, area.Y + 24, area.Width - 16, area.Height - 32), Safe64.Black1);
			}

			//Corners
			spriteBatch.Draw(Resources.Interface, new Vector2(area.X, area.Y), new Rectangle(0, 0 + 64, 16, 32), Safe64.Black4);
			spriteBatch.Draw(Resources.Interface, new Vector2(area.X + area.Width - 16, area.Y), new Rectangle(32, 0 + 64, 16, 32), Safe64.Black4);
			spriteBatch.Draw(Resources.Interface, new Vector2(area.X, area.Y + area.Height - 16), new Rectangle(0, 48 + 64, 16, 16), Safe64.Black4);
			spriteBatch.Draw(Resources.Interface, new Vector2(area.X + area.Width - 16, area.Y + area.Height - 16), new Rectangle(32, 48 + 64, 16, 16), Safe64.Black4);

			//Borders
			spriteBatch.Draw(Resources.Interface, new Rectangle(area.X + 16, area.Y, area.Width - 32, 32), new Rectangle(16, 0 + 64, 16, 32), Safe64.Black4);
			spriteBatch.Draw(Resources.Interface, new Rectangle(area.X, area.Y + 32, 16, area.Height - 48), new Rectangle(0, 32 + 64, 16, 16), Safe64.Black4);
			spriteBatch.Draw(Resources.Interface, new Rectangle(area.X + 16, area.Y + area.Height - 16, area.Width - 32, 16), new Rectangle(16, 48 + 64, 16, 16), Safe64.Black4);
			spriteBatch.Draw(Resources.Interface, new Rectangle(area.X + area.Width - 16, area.Y + 32, 16, area.Height - 48), new Rectangle(32, 32 + 64, 16, 16), Safe64.Black4);
		}

		public static void Reset()
		{
			switches = new Rectangle[4];
			stoggles = new bool[4] { true, false, false, false };
			buttons = new Rectangle[2];
			btoggles = new bool[3] { false, false, false };
			help = new Rectangle[3];
			htoggle = false;
		}
	}
}
