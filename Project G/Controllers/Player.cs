﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Controllers
{
	public static class Player
	{
		//Enum
		public enum GameplayState { MainMenu, Theory, TheoryTest, Piloting, PilotingTest };

		//Variables
		public static int TheoryQuestionsCorrect = 0;
		public static int PilotingFaults = 0;
		public static Vector3 Position = new Vector3(0, 0, 800);
		public static float Rotation = 0;
		public static Vector2 Velocity = new Vector2(0, 0);
		public static bool DisplacementActive = false;
		public static byte AlcubierreDistance = 0;
		public static bool AlcubierreActive = false;
		public static TimeSpan AlcubierreCooldown = new TimeSpan();

		public static GameplayState CurrentState = GameplayState.MainMenu;

		//Methods
		public static void ResetTheory()
		{
			TheoryQuestionsCorrect = 0;

			TheoryTest.Reset();
		}
		public static void ResetPractical()
		{
			PilotingFaults = 0;
			Position = new Vector3(0, 0, 800);
			Rotation = 0;
			Velocity = new Vector2(0, 0);
			DisplacementActive = false;
			AlcubierreDistance = 0;
			AlcubierreActive = false;
			AlcubierreCooldown = new TimeSpan();

			PilotingPractice.Reset();
		}
	}
}
