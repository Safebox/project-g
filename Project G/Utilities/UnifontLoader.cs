﻿using System.IO;
using System.Globalization;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

/// <summary>
/// Require's GNU Unifont's Plane 0 cropped to 4096 x 4096 as a base. Available at: http://unifoundry.com/unifont/index.html
/// </summary>
public class UnifontLoader
{
	//Variables
	Texture2D texture;
	Point[] characterList;
	int[] diacriticList;

	//Properties
	public Texture2D Texture
	{
		get
		{
			return texture;
		}
	}

	/// <param name="gd">Needed to load UniFont.png</param>
	/// <param name="contentFolder">Needed to find UniFont.png</param>
	public UnifontLoader(GraphicsDevice gd, string contentFolder)
	{
		texture = Texture2D.FromStream(gd, new FileStream(contentFolder + @"\UniFont.png", FileMode.Open));
		characterList = new Point[(texture.Width / 16) * (texture.Height / 16)];
		diacriticList = new int[1247];
		int d = 0;
		for (int c = 0; c < characterList.Length; c++)
		{
			characterList[c] = new Point((int)((c * 16f) % (texture.Width)), (int)((c * 16f) / (texture.Width)) * 16);
			if (CharUnicodeInfo.GetUnicodeCategory((char)c) == UnicodeCategory.NonSpacingMark || CharUnicodeInfo.GetUnicodeCategory((char)c) == UnicodeCategory.SpacingCombiningMark)
			{
				diacriticList[d] = c;
				d++;
			}
		}
	}

	//Methods
	/// <param name="scale">1 = 16 x 16</param>
	/// <param name="fullwidth">1/2 = 8 x 16</param>
	public void DrawString(SpriteBatch spriteBatch, Vector2 position, string text, Color color, float scale = 1f, bool fullwidth = false)
	{
		Vector2 posOffset = new Vector2();
		bool firstGlyphOfLine = true;

		for (int i = 0; i < text.Length; i++)
		{
			if (text[i] == '\n')
			{
				posOffset.X = 0;
				posOffset.Y += 16f * scale;
				firstGlyphOfLine = true;
			}
			else if (text[i] == '\t')
			{
				position.X += (16f * 3f) * scale * (fullwidth ? 1f : 0.5f);
			}
			else
			{
				if (firstGlyphOfLine)
				{
					posOffset.X = 0;
					firstGlyphOfLine = false;
				}
				else
				{
					bool isDiacritic = false;
					foreach (int d in diacriticList)
					{
						if (text[i] == d)
						{
							isDiacritic = true;
							break;
						}
					}
					if (!isDiacritic)
					{
						posOffset.X += 16f * scale * (fullwidth ? 1f : 0.5f);
					}
				}

				if (text[i] != '\r' && text[i] != '\n')
				{
					Rectangle charRect = CharFinder(text[i]);
					Vector2 pos = posOffset + position;
					for (int j = 0; j < 1f / scale; j++)
					{
						spriteBatch.Draw(texture, pos, charRect, color, 0f, new Vector2(), scale, SpriteEffects.None, 0f);
					}
				}
			}
		}
	}

	private Rectangle CharFinder(char c)
	{
		return new Rectangle(characterList[c], new Point(16, 16));
	}
}