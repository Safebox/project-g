﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Newtonsoft.Json;

namespace Utilities
{
	public struct SBody
	{
		//Variables
		string name;
		float mass;
		float radius;
		string parent;
		float distance;
		float orbit;

		//Properties
		public string Name
		{
			get
			{
				return name;
			}
			set
			{
				name = value;
			}
		}
		public float Mass
		{
			get
			{
				return mass;
			}
			set
			{
				mass = value;
			}
		}
		public float Radius
		{
			get
			{
				return radius;
			}
			set
			{
				radius = value;
			}
		}
		public float Gravity
		{
			get
			{
				return (float)(((6.67408 * Math.Pow(10, -11)) * mass) / Math.Pow(radius, 2));
			}
		}
		public string Parent
		{
			get
			{
				return parent;
			}
			set
			{
				parent = value;
			}
		}
		public float Distance
		{
			get
			{
				return distance;
			}
			set
			{
				distance = value;
			}
		}
		public float Orbit
		{
			get
			{
				return orbit;
			}
			set
			{
				orbit = value;
			}
		}

		//Constructors
		public SBody(string name, float mass, float radius, string parent, float distance, float orbit)
		{
			this.name = name;
			this.mass = mass;
			this.radius = radius;
			this.parent = parent;
			this.distance = distance;
			this.orbit = orbit;
		}

		//Methods
		public string WriteJSON()
		{
			return JsonConvert.SerializeObject(this, Formatting.Indented);
		}
	}
}
