﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Utilities
{
	public struct SQuestionCollection
	{
		//Variables
		SQuestion[] questions;

		//Properties
		public SQuestion[] Questions
		{
			get
			{
				return questions;
			}
			set
			{
				questions = value;
			}
		}

		//Constructors
		public SQuestionCollection(SQuestion[] questions)
		{
			this.questions = questions;
		}

		//Methods
		public void ShuffleQuestions()
		{
			Random rand = new Random();
			int n = questions.Length;
			for (int i = 0; i < n; i++)
			{
				int r = i + rand.Next(n - i);
				SQuestion t = questions[r];
				questions[r] = questions[i];
				questions[i] = t;
			}
			for (int q = 0; q < questions.Length; q++)
			{
				questions[q].ShuffleAnswers();
			}
		}
		public string WriteJSON()
		{
			return JsonConvert.SerializeObject(this, Formatting.Indented);
		}
	}
}
