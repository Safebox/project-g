﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;

public class Safe64
{
	public static Color[] Palette
	{
		get
		{
			return new Color[]
				{
					White1, White2, White3, White4,
					Black1, Black2, Black3, Black4,
					LightGray1, LightGray2, LightGray3, LightGray4,
					DarkGray1, DarkGray2, DarkGray3, DarkGray4,
					Red1, Red2, Red3, Red4,
					Orange1, Orange2, Orange3, Orange4,
					Yellow1, Yellow2, Yellow3, Yellow4,
					LightGreen1, LightGreen2, LightGreen3, LightGreen4,
					DarkGreen1, DarkGreen2, DarkGreen3, DarkGreen4,
					LightBlue1, LightBlue2, LightBlue3, LightBlue4,
					DarkBlue1, DarkBlue2, DarkBlue3, DarkBlue4,
					Indigo1, Indigo2, Indigo3, Indigo4,
					Violet1, Violet2, Violet3, Violet4,
					Pink1, Pink2, Pink3, Pink4,
					Peach1, Peach2, Peach3, Peach4,
					Brown1, Brown2, Brown3, Brown4
				};
		}
	}

	//White - Black - Gray
	public static Color White1 = new Color(255, 240, 231);
	public static Color White2 = new Color(191, 180, 174);
	public static Color White3 = new Color(128, 121, 116);
	public static Color White4 = new Color(64, 60, 58);

	public static Color Black1 = new Color(0, 0, 0);
	public static Color Black2 = new Color(64, 64, 64);
	public static Color Black3 = new Color(128, 128, 128);
	public static Color Black4 = new Color(255, 255, 255);

	public static Color LightGray1 = new Color(192, 193, 197);
	public static Color LightGray2 = new Color(145, 146, 148);
	public static Color LightGray3 = new Color(97, 97, 99);
	public static Color LightGray4 = new Color(47, 47, 48);

	public static Color DarkGray1 = new Color(95, 87, 80);
	public static Color DarkGray2 = new Color(71, 65, 60);
	public static Color DarkGray3 = new Color(48, 44, 40);
	public static Color DarkGray4 = new Color(23, 21, 19);

	//Rainbow
	public static Color Red1 = new Color(255, 7, 78);
	public static Color Red2 = new Color(191, 6, 58);
	public static Color Red3 = new Color(128, 4, 39);
	public static Color Red4 = new Color(64, 2, 20);

	public static Color Orange1 = new Color(255, 161, 8);
	public static Color Orange2 = new Color(191, 120, 6);
	public static Color Orange3 = new Color(128, 80, 4);
	public static Color Orange4 = new Color(64, 40, 2);

	public static Color Yellow1 = new Color(254, 235, 44);
	public static Color Yellow2 = new Color(191, 178, 32);
	public static Color Yellow3 = new Color(128, 119, 22);
	public static Color Yellow4 = new Color(64, 60, 11);

	public static Color LightGreen1 = new Color(0, 227, 57);
	public static Color LightGreen2 = new Color(0, 171, 43);
	public static Color LightGreen3 = new Color(0, 115, 29);
	public static Color LightGreen4 = new Color(0, 56, 14);

	public static Color DarkGreen1 = new Color(0, 133, 81);
	public static Color DarkGreen2 = new Color(0, 99, 61);
	public static Color DarkGreen3 = new Color(0, 66, 41);
	public static Color DarkGreen4 = new Color(0, 33, 20);

	public static Color LightBlue1 = new Color(44, 171, 254);
	public static Color LightBlue2 = new Color(32, 127, 191);
	public static Color LightBlue3 = new Color(22, 85, 128);
	public static Color LightBlue4 = new Color(11, 43, 64);

	public static Color DarkBlue1 = new Color(34, 46, 83);
	public static Color DarkBlue2 = new Color(26, 36, 64);
	public static Color DarkBlue3 = new Color(18, 24, 43);
	public static Color DarkBlue4 = new Color(8, 11, 20);

	public static Color Indigo1 = new Color(130, 117, 154);
	public static Color Indigo2 = new Color(97, 87, 115);
	public static Color Indigo3 = new Color(64, 58, 76);
	public static Color Indigo4 = new Color(32, 29, 38);

	public static Color Violet1 = new Color(125, 41, 83);
	public static Color Violet2 = new Color(94, 31, 62);
	public static Color Violet3 = new Color(64, 21, 43);
	public static Color Violet4 = new Color(31, 10, 21);

	//Mixes
	public static Color Pink1 = new Color(255, 118, 166);
	public static Color Pink2 = new Color(191, 88, 124);
	public static Color Pink3 = new Color(128, 59, 83);
	public static Color Pink4 = new Color(64, 29, 41);

	public static Color Peach1 = new Color(255, 202, 168);
	public static Color Peach2 = new Color(191, 151, 126);
	public static Color Peach3 = new Color(128, 101, 84);
	public static Color Peach4 = new Color(64, 51, 42);

	public static Color Brown1 = new Color(169, 82, 56);
	public static Color Brown2 = new Color(128, 62, 42);
	public static Color Brown3 = new Color(84, 41, 28);
	public static Color Brown4 = new Color(43, 21, 14);
}