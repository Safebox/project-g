﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Utilities
{
	public struct SQuestion
	{
		//Variables
		string question;
		Dictionary<int, string> answers;
		string[] shuffledAnswers;

		//Properties
		public string Question
		{
			get
			{
				return question;
			}
			set
			{
				question = value;
			}
		}
		public Dictionary<int, string> Answers
		{
			get
			{
				return answers;
			}
			set
			{
				answers = value;
				this.shuffledAnswers = answers.Values.ToArray();
			}
		}
		public string[] ShuffledAnswers
		{
			get
			{
				return shuffledAnswers;
			}
		}

		//Constructors
		public SQuestion(string question, Dictionary<int, string> answers)
		{
			this.question = question;
			this.answers = answers;
			this.shuffledAnswers = answers.Values.ToArray();
		}

		//Methods
		public void ShuffleAnswers()
		{
			Random rand = new Random();
			int n = shuffledAnswers.Length;
			for (int i = 0; i < n; i++)
			{
				int r = i + rand.Next(n - i);
				string t = shuffledAnswers[r];
				shuffledAnswers[r] = shuffledAnswers[i];
				shuffledAnswers[i] = t;
			}
		}
		public string WriteJSON()
		{
			return JsonConvert.SerializeObject(this, Formatting.Indented);
		}
	}
}
