﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Newtonsoft.Json;

namespace Utilities
{
	public struct SStarSystem
	{
		//Variables
		SBody[] bodies;
		static float strengthConstant;

		//Properties
		public SBody[] Bodies
		{
			get
			{
				return bodies;
			}
			set
			{
				bodies = value;
			}
		}
		public SBody[] Planets
		{
			get
			{
				List<SBody> p = new List<SBody>();
				foreach (SBody b in bodies)
				{
					if (b.Parent == "B_Sol")
					{
						p.Add(b);
					}
				}
				return p.ToArray();
			}
		}
		public static float StrengthConstant
		{
			get
			{
				return strengthConstant;
			}
			set
			{
				strengthConstant = value;
			}
		}

		//Constructors
		public SStarSystem(SBody[] bodies)
		{
			this.bodies = bodies;
		}

		//Methods
		public Vector3[] BodyPositions()
		{
			return BodyPositions(1);
		}
		public Vector3[] BodyPositions(float maxRadius)
		{
			Vector3[] p = new Vector3[bodies.Length];
			p[0] = new Vector3(0, 0, 1);
			for (int b = 1; b < bodies.Length; b++)
			{
				SBody body = bodies[b];
				Vector2 bodyPos = new Vector2((float)Math.Cos(TimeSpan.FromTicks(DateTime.UtcNow.Ticks).TotalDays / (body.Orbit / 2f) * 3.14f) * (body.Distance / maxRadius), (float)Math.Sin(TimeSpan.FromTicks(DateTime.UtcNow.Ticks).TotalDays / (body.Orbit / 2f) * 3.14f) * (body.Distance / maxRadius));
				if (body.Parent != "B_Sol")
				{
					SBody parent = this[body.Parent];
					Vector2 parentPos = new Vector2((float)Math.Cos(TimeSpan.FromTicks(DateTime.UtcNow.Ticks).TotalDays / (parent.Orbit / 2f) * 3.14f) * (parent.Distance / maxRadius), (float)Math.Sin(TimeSpan.FromTicks(DateTime.UtcNow.Ticks).TotalDays / (parent.Orbit / 2f) * 3.14f) * (parent.Distance / maxRadius));
					p[b] = new Vector3(parentPos.X + bodyPos.X, parentPos.Y + bodyPos.Y, body.Gravity / bodies[0].Gravity);
				}
				else
				{
					p[b] = new Vector3(bodyPos.X, bodyPos.Y, body.Gravity / bodies[0].Gravity);
				}
			};
			return p;
		}
		public Vector3[] BodyPositions(Vector2 playerPos, float playerRot)
		{
			/*Vector3[] p = new Vector3[bodies.Length];
			p[0] = new Vector3(0 - playerPos.X, 0 - playerPos.Y, 1);
			for (int b = 1; b < bodies.Length; b++)
			{
				SBody body = bodies[b];
				Vector2 bodyPos = new Vector2((float)Math.Cos(TimeSpan.FromTicks(DateTime.UtcNow.Ticks).TotalDays / (body.Orbit / 2f) * 3.14f) * body.Distance, (float)Math.Sin(TimeSpan.FromTicks(DateTime.UtcNow.Ticks).TotalDays / (body.Orbit / 2f) * 3.14f) * body.Distance);
				if (body.Parent != "B_Sol")
				{
					SBody parent = this[body.Parent];
					Vector2 parentPos = new Vector2((float)Math.Cos(TimeSpan.FromTicks(DateTime.UtcNow.Ticks).TotalDays / (parent.Orbit / 2f) * 3.14f) * parent.Distance, (float)Math.Sin(TimeSpan.FromTicks(DateTime.UtcNow.Ticks).TotalDays / (parent.Orbit / 2f) * 3.14f) * parent.Distance);
					p[b] = new Vector3(parentPos.X + bodyPos.X - playerPos.X, parentPos.Y + bodyPos.Y - playerPos.Y, body.Gravity / bodies[0].Gravity);
				}
				else
				{
					p[b] = new Vector3(bodyPos.X - playerPos.X, bodyPos.Y - playerPos.Y, body.Gravity / bodies[0].Gravity);
				}
			};
			return p;*/
			return BodyPositions(playerPos, playerRot, new Vector2(0, 0), 1);
		}
		public Vector3[] BodyPositions(Vector2 playerPos, float playerRot, Vector2 offset, float radius)
		{
			Vector3[] p = new Vector3[bodies.Length];
			p[0] = new Vector3((0 - playerPos.X) / radius + offset.X, (0 - playerPos.Y) / radius - offset.Y, bodies[0].Gravity / bodies[0].Gravity);
			for (int b = 1; b < bodies.Length; b++)
			{
				SBody body = bodies[b];
				Vector2 bodyPos = new Vector2((float)Math.Cos((TimeSpan.FromTicks(DateTime.UtcNow.Ticks).TotalDays - playerRot) / (body.Orbit / 2f) * 3.14f) * body.Distance, (float)Math.Sin((TimeSpan.FromTicks(DateTime.UtcNow.Ticks).TotalDays - playerRot) / (body.Orbit / 2f) * 3.14f) * body.Distance);
				if (body.Parent != "B_Sol")
				{
					SBody parent = this[body.Parent];
					Vector2 parentPos = new Vector2((float)Math.Cos((TimeSpan.FromTicks(DateTime.UtcNow.Ticks).TotalDays - playerRot) / (parent.Orbit / 2f) * 3.14f) * parent.Distance, (float)Math.Sin((TimeSpan.FromTicks(DateTime.UtcNow.Ticks).TotalDays - playerRot) / (parent.Orbit / 2f) * 3.14f) * parent.Distance);
					p[b] = new Vector3((parentPos.X + bodyPos.X - playerPos.X) / radius + offset.X, (parentPos.Y + bodyPos.Y - playerPos.Y) / radius - offset.Y, body.Gravity / bodies[0].Gravity);// / bodies[0].Gravity);
				}
				else
				{
					p[b] = new Vector3((bodyPos.X - playerPos.X) / radius + offset.X, (bodyPos.Y - playerPos.Y) / radius - offset.Y, body.Gravity / bodies[0].Gravity);
				}
			};
			return p;
		}
		public string WriteJSON()
		{
			return JsonConvert.SerializeObject(this, Formatting.Indented);
		}

		//Operators
		public SBody this[string key]
		{
			get
			{
				foreach (SBody b in bodies)
				{
					if (b.Name == key)
					{
						return b;
					}
				};
				return new SBody();
			}
		}
	}
}
