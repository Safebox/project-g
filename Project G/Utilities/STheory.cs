﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Utilities
{
	public struct STheory
	{
		//Variables
		Dictionary<string, string> sections;

		//Properties
		public Dictionary<string, string> Sections
		{
			get
			{
				return sections;
			}
			set
			{
				sections = value;
			}
		}

		//Constructors
		public STheory(Dictionary<string, string> sections)
		{
			this.sections = sections;
		}

		//Methods
		public string WriteJSON()
		{
			return JsonConvert.SerializeObject(this, Formatting.Indented);
		}
	}
}
